import React from "react";
import Home from "../Home";
import { shallow } from "enzyme";

it("renders correctly enzyme", () => {
  const wrapper = shallow(<Home />);

  expect(wrapper).toMatchSnapshot();
});
