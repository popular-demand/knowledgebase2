import React from "react";
import Router from "../Router";
import { shallow } from "enzyme";

it("renders correctly enzyme", () => {
  const wrapper = shallow(<Router />);

  expect(wrapper).toMatchSnapshot();
});
