import React from "react";
import MainContent from "../MainContent";
import { Provider } from "react-redux";
import store from "../../../store/index";

import { mount } from "enzyme";

it("renders correctly enzyme", () => {
  const wrapper = mount(
    <Provider store={store}>
      <MainContent />
    </Provider>
  );

  expect(wrapper).toMatchSnapshot();
});
