import React from "react";
import ContentRow from "../ContentRow";
import content from "../../../store/displayData/content";
import { shallow } from "enzyme";

it("renders correctly enzyme", () => {
  const wrapper = shallow(<ContentRow data={content} />);

  expect(wrapper).toMatchSnapshot();
});
