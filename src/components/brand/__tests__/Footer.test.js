import React from "react";
import Footer from "../Footer";

import { shallow } from "enzyme";

it("renders correctly enzyme", () => {
  const wrapper = shallow(<Footer />);

  expect(wrapper).toMatchSnapshot();
});
