import React from "react";
import Navbar from "../Navbar";
import { Router } from "react-router-dom";

import { mount } from "enzyme";

it("renders correctly enzyme", () => {
  const testHistory = { location: {}, listen: () => {} };
  const wrapper = mount(<Router history={testHistory}><Navbar /></Router>);

  expect(wrapper.find("#mainNav")).toMatchSnapshot();
});
