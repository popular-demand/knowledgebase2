import React from "react";
import Masthead from "../Masthead";

import { shallow } from "enzyme";

it("renders correctly enzyme", () => {
  const background = "home-bg.jpg";
  const wrapper = shallow(
    <Masthead background={background} subhead="Testing Subheading Message" />
  );

  expect(wrapper).toMatchSnapshot();
});
