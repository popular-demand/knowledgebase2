import React from "react";

import reducer from "../../store/displayData/reducer";
import { initialState } from "../../store/displayData/reducer";

it("should return the initial state", () => {
  expect(reducer(undefined, {})).toEqual(initialState);
});
