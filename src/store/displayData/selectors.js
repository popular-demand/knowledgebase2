import { createSelector } from "reselect";
import { filter, get } from "lodash";

export default createSelector(
  (state) => filter(get(state, "content"), { category: "technology" }),
  (state) => filter(get(state, "content"), { category: "design" }),
  (techContent, designContent) => ({ techContent, designContent })
);